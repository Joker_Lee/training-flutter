import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'dart:io';

class LoadFileConsoleBloc {
  StreamController _fullNameController = new StreamController();
  StreamController _statusController = new StreamController();
  StreamController _bioController = new StreamController();

  Stream get fullNameStream => _fullNameController.stream;

  Stream get statusStream => _statusController.stream;

  Stream get bioStream => _bioController.stream;

  read() async {
    String _text;
    try {
      final directory = await getApplicationDocumentsDirectory();
      final file = File('${directory.path}/userName.txt');
      _text = await file.readAsString();
      _fullNameController.sink.add("${_text.split("/")[0]}");
      _statusController.sink.add("${_text.split("/")[1]}");
      _bioController.sink.add("${_text.split("/")[2]}");
    } catch (e) {
      print("Couldn't read file");
    }
  }

  void dispose() {
    _fullNameController.close();
    _statusController.close();
    _bioController.close();
  }
}
