import 'package:path_provider/path_provider.dart';
import 'dart:io';

class WriteLocalFileBloc {
  /* write file */
  write(String userName, String status, String bio) async {
    delete();
    final directory = await getApplicationDocumentsDirectory();
    print("Path File: " + directory.path);
    final file = File('${directory.path}/userName.txt');
    /* add Text in file */
    await file.writeAsString(userName + "/" + status + "/" + bio);
  }
}

/* Delete file */
void delete() async {
  try {
    final directory = await getApplicationDocumentsDirectory();
    final dir = Directory('${directory.path}/userName.txt');
    dir.deleteSync(recursive: true);
    print("Delete succses");
  } catch (e) {
    print("No file");
  }
}
