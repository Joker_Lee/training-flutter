import 'dart:async';
import 'package:weather/weather.dart';
import '../validators/Validators.dart';

// ignore: camel_case_types
class WeatherBloc {
  WeatherStation ws;
  StreamController _locationController = new StreamController();
  StreamController _weatherIconController = new StreamController();
  StreamController _weatherController = new StreamController();
  StreamController _temperatureController = new StreamController();
  StreamController _dateController = new StreamController();
  StreamController _timeController = new StreamController();
  StreamController _keyAPIWeatherController = new StreamController();

  Stream get locationStream => _locationController.stream;

  Stream get weatherStream => _weatherController.stream;

  Stream get weatherIconStream => _weatherIconController.stream;

  Stream get temperatureStream => _temperatureController.stream;

  Stream get dateStream => _dateController.stream;

  Stream get timeStream => _timeController.stream;

  bool isNullKeyAPIWeather(String keyAPIWeather) {
    if (!ValidKeyAPI.isValidKeyAPI(keyAPIWeather)) {
      _keyAPIWeatherController.sink.addError("Please add a key API");
      return false;
    }
    ws = new WeatherStation(keyAPIWeather);
    initPlatformState();
    return true;
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    queryWeather();
  }

  void queryWeather() async {
    Weather w = await ws.currentWeather();
    _locationController.sink.add('${w.country}');
    _weatherIconController.sink.add(w.weatherIcon);
    _temperatureController.sink
        .add(w.temperature.celsius.ceil().toString() + "°C");
    _weatherController.sink.add(w.weatherMain);
    setDateTime();
  }

  void setDateTime() {
    var currentDate = new DateTime.now();
    var _weekDate = '';
    var _month = '';
    var _day = '';
    var _hour = '';
    var _minute = '';

    switch (currentDate.weekday) {
      case 1:
        _weekDate = 'Monday';
        break;
      case 2:
        _weekDate = 'Tuesday';
        break;
      case 3:
        _weekDate = 'Wednesday';
        break;
      case 4:
        _weekDate = 'Thursday';
        break;
      case 5:
        _weekDate = 'Friday';
        break;
      case 6:
        _weekDate = 'Saturday';
        break;
      case 7:
        _weekDate = 'Sunday';
        break;
    }

    switch (currentDate.month) {
      case 1:
        _month = 'January';
        break;
      case 2:
        _month = 'February';
        break;
      case 3:
        _month = 'March';
        break;
      case 4:
        _month = 'April';
        break;
      case 5:
        _month = 'May';
        break;
      case 6:
        _month = 'June';
        break;
      case 7:
        _month = 'July';
        break;
      case 8:
        _month = 'August';
        break;
      case 9:
        _month = 'September';
        break;
      case 10:
        _month = 'October';
        break;
      case 11:
        _month = 'November';
        break;
      case 12:
        _month = 'December';
        break;
    }

    if (currentDate.day == 1 ||
        currentDate.day == 21 ||
        currentDate.day == 31) {
      _day = '${currentDate.day}st';
    } else if (currentDate.day == 2 || currentDate.day == 22) {
      _day = '${currentDate.day}nd';
    } else if (currentDate.day == 3 || currentDate.day == 23) {
      _day = '${currentDate.day}rd';
    } else {
      _day = '${currentDate.day}th';
    }

    if (currentDate.hour < 10) {
      _hour = '0${currentDate.hour}';
    } else {
      _hour = currentDate.hour.toString();
    }

    if (currentDate.minute < 10) {
      _minute = '0${currentDate.minute}';
    } else {
      _minute = currentDate.minute.toString();
    }

    _dateController.sink
        .add('$_weekDate, $_month $_day, ${currentDate.year}');
    _timeController.sink.add('$_hour:$_minute');
  }

  void dispose() {
    _locationController.close();
    _weatherIconController.close();
    _weatherController.close();
    _temperatureController.close();
    _dateController.close();
    _timeController.close();
    _keyAPIWeatherController.close();
  }
}
