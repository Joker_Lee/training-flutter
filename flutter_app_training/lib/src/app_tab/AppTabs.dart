// import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import '../page/HomePage.dart' as Home;
import '../page/ProfilePage.dart' as Profile;
import '../page/SchedulePage.dart' as Schedule;
import '../page/HeathPage.dart' as Heath;

class AppTabs extends StatefulWidget {
  @override
  MyTabsState createState() => new MyTabsState();
}

class MyTabsState extends State<AppTabs> with SingleTickerProviderStateMixin {
  final GoogleSignIn googleSignIn = GoogleSignIn();
  var _flagLogIn;
  TabController controller;

  @override
  void initState() {
    super.initState();
    controller = new TabController(length: 4, vsync: this);
    checkUser();
  }

  void checkUser() async{
    bool _isSignedIn = await googleSignIn.isSignedIn();
    print("isSignedIn: $_isSignedIn");
    if (_isSignedIn) {
      setState(() {
        _flagLogIn = 1;
      });
    } else {
      setState(() {
        _flagLogIn = 0;
      });
    }
    print("_flagLogIn: $_flagLogIn");
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      bottomNavigationBar: new Material(
        color: Color(0xFF1C0746).withOpacity(0.75),
        /*54*/
        child: new TabBar(controller: controller, tabs: <Tab>[
          new Tab(
            icon: new Icon(Icons.home),
            text: "Home",
          ),
          new Tab(
            icon: new Icon(Icons.person),
            text: "Profile",
          ),
          new Tab(
            icon: new Icon(Icons.schedule),
            text: "Schedule",
          ),
          new Tab(
            icon: new Icon(Icons.favorite),
            text: "Health",
          ),
        ]),
      ),
      body: new TabBarView(controller: controller, children: <Widget>[
        new Home.HomePage(),
        new Profile.UserProfilePage(),
        new Home.HomePage(),
        new Home.HomePage(),
      ]),
    );
  }
}
