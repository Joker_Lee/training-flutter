import 'package:flutter_app_training/src/blocs/WriteLocalFileBloc.dart';
import 'package:flutter_app_training/src/blocs/LoadFileConsoleBloc.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditProfile extends StatefulWidget {
  @override
  EditProfileState createState() =>
      new EditProfileState();
}

class EditProfileState extends State<EditProfile> {
  LoadFileConsoleBloc loadFileConsoleBloc = new LoadFileConsoleBloc();
  WriteLocalFileBloc writeLocalFile = new WriteLocalFileBloc();
  File _image;
  final TextEditingController _userName = new TextEditingController();
  final TextEditingController _status = new TextEditingController();
  final TextEditingController _bio = new TextEditingController();

  @override
  void initState() {
    super.initState();
    _readImage();
    loadFileConsoleBloc.read();
    loadFileConsoleBloc.fullNameStream.listen((fullName)=>_userName.text = fullName);
    loadFileConsoleBloc.statusStream.listen((status)=>_status.text = status);
    loadFileConsoleBloc.bioStream.listen((bio)=>_bio.text = bio);

  }

  @override
  void dispose() {
    loadFileConsoleBloc.dispose();
    super.dispose();
  }

  Future getImage() async {
    // ignore: deprecated_member_use
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
//    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (image != null && image.path != null) {
      setState(() {
        _image = image;
      });
    }
  }

  void _saveImage() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('avatarPath', _image.path);
    final avatarPath = prefs.getString('avatarPath') ?? '';
    print('avatar path: $avatarPath');
  }

  void _readImage() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      final avatarPath = prefs.getString('avatarPath');
      var file;
      if (avatarPath != null) {
        file = new File(avatarPath);
        Image.file(file);
        _image = file;
      }
    });
  }

  String _urlImageAvatar = "assets/images/avatar.JPG";
  String _urlImageBackground = "assets/images/backgroundprofile.jpg";

  Widget _buildCoverImage(Size screenSize) {
    return Container(
      height: screenSize.height / 1,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(_urlImageBackground),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _inputForm() {
    return Container(
      padding: const EdgeInsets.all(30.0),
      color: Colors.white70,
      child: new Center(
        child: new Column(children: [
          _image == null
              ? Container(
            width: 140.0,
            height: 140.0,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(_urlImageAvatar),
              ),
              borderRadius: BorderRadius.circular(80.0),
              border: Border.all(
                color: Colors.white,
                width: 10.0,
              ),
            ),
            // ignore: deprecated_member_use
            child: FlatButton(
              child: Icon(Icons.add_a_photo, color: Colors.white70),
              onPressed: () {
                getImage();
              },
            ),
          )
              : Container(
            width: 140.0,
            height: 140.0,
            decoration: BoxDecoration(
              image: new DecorationImage(
                image: new FileImage(_image),
                fit: BoxFit.cover,
              ),
              borderRadius: BorderRadius.circular(80.0),
              border: Border.all(
                color: Colors.white,
                width: 10.0,
              ),
            ),
            // ignore: deprecated_member_use
            child: FlatButton(
              child: Icon(Icons.add_a_photo, color: Colors.white70),
              onPressed: () {
                getImage();
              },
            ),
          ),
          new Padding(padding: EdgeInsets.only(top: 10.0)),
          TextFormField(
            maxLength: 20,
            controller: _userName,
            decoration: new InputDecoration(
              icon: Icon(Icons.person),
              labelText: "Enter your name",
              fillColor: Colors.white,
              border: new OutlineInputBorder(
                borderRadius: new BorderRadius.circular(25.0),
                borderSide: new BorderSide(),
              ),
              //fillColor: Colors.green
            ),
          ),
          new Padding(padding: EdgeInsets.only(top: 10.0)),
          new TextFormField(
            maxLength: 20,
            controller: _status,
            decoration: new InputDecoration(
              icon: Icon(Icons.assignment_turned_in),
              labelText: "Enter your status",
              fillColor: Colors.white,
              border: new OutlineInputBorder(
                borderRadius: new BorderRadius.circular(25.0),
                borderSide: new BorderSide(),
              ),
              //fillColor: Colors.green
            ),
          ),
          new Padding(padding: EdgeInsets.only(top: 10.0)),
          new TextFormField(
            maxLength: 50,
            controller: _bio,
            decoration: new InputDecoration(
              icon: Icon(Icons.book),
              labelText: "Enter your bio",
              fillColor: Colors.white,
              border: new OutlineInputBorder(
                borderRadius: new BorderRadius.circular(25.0),
                borderSide: new BorderSide(),
              ),
              //fillColor: Colors.green
            ),
          ),
          new Padding(padding: EdgeInsets.only(top: 50.0)),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        // ignore: deprecated_member_use
                        child: RaisedButton(
                          color: Color.fromRGBO(68, 84, 99, 10),
                          shape: StadiumBorder(),
                          child: Text(
                            'Save',
                            style: TextStyle(fontSize: 24, color: Colors.white),
                          ),
                          onPressed: () {
                            writeLocalFile.write(_userName.text, _status.text, _bio.text);
                            /* delete();
                            _write(_userName.text, _status.text, _bio.text);*/
                            _saveImage();
                            Navigator.pop(context);
                          },
                        )
                    ),
                    SizedBox(width: 10.0),
                    Expanded(
                        // ignore: deprecated_member_use
                        child: RaisedButton(
                          shape: StadiumBorder(),
                          child: Text(
                            'Clear',
                            style: TextStyle(fontSize: 24, color: Colors.black45),
                          ),
                          onPressed: () {
                            //checkPath();
                            _userName.clear();
                            _status.clear();
                            _bio.clear();
                          },
                        ))
                  ],
                ),
              ),
            ),
          )
        ]),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: new Text("Edit Profile"),
        backgroundColor: Color.fromRGBO(68, 84, 99, 10),
      ),
      body: Stack(
        children: <Widget>[
          _buildCoverImage(screenSize),
          SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  //SizedBox(height: screenSize.height / 6.4),
                  //_buildProfileImage(),
                  _inputForm(),
                  //_buildButtons()
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

