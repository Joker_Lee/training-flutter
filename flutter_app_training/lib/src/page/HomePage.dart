import '../blocs/WeatherBloc.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  WeatherBloc weatherBloc = new WeatherBloc();
  String _key = '32367c10951289b14b19a306e65e3558';

  @override
  void initState() {
    super.initState();
    weatherBloc.isNullKeyAPIWeather(_key);
  }
  @override
  void dispose() {
    weatherBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: new Text("Weather Your Location "),
        backgroundColor:  Color(0xFF1C0746).withOpacity(0.75),
        actions: <Widget>[
          FlatButton(
            child: Icon(Icons.power_settings_new, color: Colors.white),
          )
        ],

      ),
      body: SingleChildScrollView(
        child: Container(
          height: 550,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/cloud.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Hi",
                  style: new TextStyle(color: Colors.white),
                ),
                Text("LKHN",
                    style: new TextStyle(
                        color: Colors.white,
                        fontSize: 40.0,
                        fontFamily: "Barlow")),
                Text("********************************",
                    style: new TextStyle(color: Colors.white)),
                StreamBuilder(
                  stream: weatherBloc.locationStream,
                  builder: (context, snapshot) => Text(snapshot.data.toString(),
                      style:
                      new TextStyle(color: Colors.white, fontSize: 22.0)),
                ),
                StreamBuilder(
                  stream: weatherBloc.weatherStream,
                  builder: (context, snapshot) => Text(snapshot.data.toString(),
                      style:
                      new TextStyle(color: Colors.white, fontSize: 32.0)),
                ),
                StreamBuilder(
                  stream: weatherBloc.temperatureStream,
                  builder: (context, snapshot) => Text(snapshot.data.toString(),
                      style: new TextStyle(color: Colors.white)),
                ),
                StreamBuilder(
                  stream: weatherBloc.weatherIconStream,
                  builder: (context, snapshot) => Image.network(
                      'https://openweathermap.org/img/wn/${snapshot.data.toString()}@2x.png'),
                ),
                StreamBuilder(
                  stream: weatherBloc.dateStream,
                  builder: (context, snapshot) =>
                      Text(snapshot.data.toString(), style: new TextStyle(color: Colors.white)),
                ),
                StreamBuilder(
                  stream: weatherBloc.timeStream,
                  builder: (context, snapshot) =>
                      Text(snapshot.data.toString(), style: new TextStyle(color: Colors.white)),
                ),
                FlatButton(
                  onPressed: onClickRefresh,
                  child: Icon(
                    Icons.refresh,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  void onClickRefresh(){
    weatherBloc.isNullKeyAPIWeather(_key);
  }
}
