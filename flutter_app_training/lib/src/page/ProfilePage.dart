import 'package:flutter/material.dart';
import 'package:flutter_app_training/src/blocs/LoadFileConsoleBloc.dart';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'EditProfilePage.dart' as EditPrf;

class UserProfilePage extends StatefulWidget {
  @override
  _UserProfilePageState createState() => _UserProfilePageState();
}

class _UserProfilePageState extends State<UserProfilePage> {
  LoadFileConsoleBloc loadFileConsoleBloc = new LoadFileConsoleBloc();
  File _image;
  String _fullName = "";
  String _status = "";
  String _bio = "";

  String _follower = "6789";

  String _following = "6666";

  String _urlImageAvatar = "assets/images/avatar.JPG";

  String _urlImageBackground = "assets/images/backgroundprofile.jpg";

  @override
  void initState() {
    super.initState();
    loadFileConsoleBloc.read();
    /*Timer.periodic(Duration(seconds: 0), (Timer t) => loadFileConsoleBloc.read());*/
    _readImage();
  }

  @override
  void dispose() {
    loadFileConsoleBloc.dispose();
    super.dispose();
  }

  void _readImage() async {
    final prefs = await SharedPreferences.getInstance();
    if (this.mounted) {
      setState(() {
        final avatarPath = prefs.getString('avatarPath');
        var file;
        if (avatarPath != null) {
          file = new File(avatarPath);
          Image.file(file);
          _image = file;
        }
      });
    }
  }

  Widget _buildCoverImage(Size screenSize) {
    return Container(
      height: screenSize.height / 4,
      /*3.7*/
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(_urlImageBackground),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _buildProfileImage() {
    return Center(
        child: Column(
          children: <Widget>[
            _image == null
                ? Container(
              width: 140.0,
              height: 140.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(_urlImageAvatar),
                ),
                borderRadius: BorderRadius.circular(80.0),
                border: Border.all(
                  color: Colors.white,
                  width: 10.0,
                ),
              ),
            )
                : Container(
              width: 140.0,
              height: 140.0,
              decoration: BoxDecoration(
                image: new DecorationImage(
                  image: new FileImage(_image),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(80.0),
                border: Border.all(
                  color: Colors.white,
                  width: 10.0,
                ),
              ),
            ),
          ],
        ));
  }

  Widget _buildFullName() {
    TextStyle _nameTextStyle = TextStyle(
      fontFamily: 'Courgette',
      color: Colors.black,
      fontSize: 25.0,
      fontWeight: FontWeight.w700,
    );

    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 50.0),
      child: Container(
        //color: Colors.red,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            StreamBuilder(
              stream: loadFileConsoleBloc.fullNameStream,
              builder:  (context, snapshot) =>
                  Container(
                    // color: Colors.green,
                    alignment: Alignment(0.0, 0.0),
                    child: Text(
                      snapshot.data != null ? snapshot.data.toString() : _fullName,
                      style: _nameTextStyle,
                      textAlign: TextAlign.center,
                    ),
                  ),
            ),
            //FlatButton(onPressed: goEditProfilePage , child: Icon(Icons.border_color, color: Colors.black, size: 20,),),
          ],
        ),
      ),
    );
  }

  Widget _buildStatus(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 6.0),
      decoration: BoxDecoration(
//        color: Theme.of(context).scaffoldBackgroundColor,
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: StreamBuilder(
        stream: loadFileConsoleBloc.statusStream,
        builder:  (context, snapshot) =>
            Text(
              snapshot.data != null ? snapshot.data.toString() : _status,
              style: TextStyle(
                fontFamily: 'Barlow',
                color: Colors.black,
                fontSize: 20.0,
                fontWeight: FontWeight.w300,
              ),
            ),
      ),
    );
  }

  Widget _buildStatItem(String label, String count) {
    TextStyle _statLabelTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 16.0,
      fontWeight: FontWeight.w200,
    );

    TextStyle _statCountTextStyle = TextStyle(
      color: Colors.black54,
      fontSize: 24.0,
      fontWeight: FontWeight.bold,
    );

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          count,
          style: _statCountTextStyle,
        ),
        Text(
          label,
          style: _statLabelTextStyle,
        ),
      ],
    );
  }

  Widget _buildStatContainer() {
    return Container(
      height: 60.0,
      margin: EdgeInsets.only(top: 8.0),
      decoration: BoxDecoration(
        color: Color(0xFFEFF4F7),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _buildStatItem("Follower", _follower),
          _buildStatItem("Following", _following),
        ],
      ),
    );
  }

  Widget _buildBio(BuildContext context) {
    TextStyle bioTextStyle = TextStyle(
      fontFamily: 'Spectral',
      fontWeight: FontWeight.w400,
      //try changing weight to w500 if not thin
      fontStyle: FontStyle.italic,
      color: Color(0xFF799497),
      fontSize: 16.0,
    );

    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      padding: EdgeInsets.all(8.0),
      child:StreamBuilder(
        stream: loadFileConsoleBloc.bioStream,
        builder: (context, snapshot) =>
            Text(
              snapshot.data != null ? snapshot.data.toString() : _bio,
              textAlign: TextAlign.center,
              style: bioTextStyle,
            ),),

    );
  }

  Widget _buildButtons() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              child: Center(
                child: Image.asset(
                  'assets/images/iconfacebook.png',
                  width: 50,
                ),
              ),
            ),
          ),
          SizedBox(width: 10.0),
          Expanded(
            child: Container(
              child: Center(
                child: Image.asset(
                  'assets/images/iconinstagram.jpg',
                  width: 50,
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: Center(
                child: Image.asset(
                  'assets/images/icontwitter.png',
                  width: 50,
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: Center(
                child: Image.asset(
                  'assets/images/iconzalo.png',
                  width: 50,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      //backgroundColor: Color.fromRGBO(68, 84, 99, 10),
      appBar: new AppBar(
        centerTitle: true,
        title: new Text("Profile"),
        backgroundColor: Color(0xFF8E24AA).withOpacity(0.75),
        actions: <Widget>[
          // ignore: deprecated_member_use
          FlatButton(
            //color: Colors.red,
            child: Icon(Icons.create, color: Colors.white),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => EditPrf.EditProfile(
                      )));
            },
          ),
        ],
      ),
      body: Stack(
        children: <Widget>[
          _buildCoverImage(screenSize),
          SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(height: screenSize.height / 8),
                  _buildProfileImage(),
                  _buildFullName(),
                  _buildStatus(context),
                  _buildStatContainer(),
                  SizedBox(height: 8.0),
                  _buildBio(context),
                  SizedBox(height: 10.0),
                  SizedBox(height: 8.0),
                  _buildButtons(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
