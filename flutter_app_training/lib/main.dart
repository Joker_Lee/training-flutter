import 'package:flutter/material.dart';
import 'package:flutter_app_training/src/app_tab/AppTabs.dart';

void main() => runApp(new MaterialApp(
    home: new AppTabs()
));